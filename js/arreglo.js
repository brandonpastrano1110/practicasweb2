const btnNum = document.getElementById('btnNum');
const selNum = document.getElementById('selNum');
const pPar = document.getElementById('pPar');
const pImp = document.getElementById('pImp');
const res = document.getElementById('res');
let arrNum = [];

btnNum.addEventListener("click", function () {
    let inNum = document.getElementById('inNum').value;
    if(inNum==0 || inNum==""){
        alert("Llena todo los campos");  
        return 0;
    }
    getAlt(inNum);
    selNum.innerHTML = "";

    arrNum.forEach(function (valor, indice) {
        const option = document.createElement("option");
        option.value = indice;
        option.text = valor;
        selNum.appendChild(option);
    });
    if (getDif(inNum) == -1)
        res.value="Asimetrico";
    else
        res.value="Simetrico";
    pPar.value=getPar(inNum);
    pImp.value=getImp(inNum);
});

function getAlt(num) {
    arrNum = [];
    for (let i = 0; i < num; i++) {
        arrNum[i] = (Math.random() * (num * 2)).toFixed(0);
    }
}
/*Funcion para comparar si el generador de numeros aleatoreo es simetrico o asimetrico
es decir: si la diferencia entre pares e impares es mayor 20%*/
function getDif(num) {
    let cPar = 0, cImp = 0;
    for (let i = 0; i < num; i++) {
        if (arrNum[i] % 2 === 0)
            cPar++;
        else
            cImp++;
    }
    let por = Math.abs((cPar - cImp) / num) * 100;
    if (por > 20)
        return -1;
    else
        return 0;
}
function getPar(num) { 
    let par=0;
    for (let i = 0; i < num; i++) {
        if (arrNum[i] % 2 == 0)
            par++;
    }
    return ((par/num)*100).toFixed(2);
}
function getImp(num) {
    let imp=0;
    for (let i = 0; i < num; i++) {
        if (arrNum[i] % 2 != 0)
            imp++; 
    }
    return ((imp/num)*100).toFixed(2);
}

/*Declarar arreglo con elementos enteros*/
let arreglo = [4, 49, 30, 10, 34, 89, 10, 1, 8, 28]
function mostrarArray(arreglo) {
    let tamaño = arreglo.length;
    for (let con = 0; con < arreglo.length; ++con) {
        console.log(con + ":" + arreglo[con]);
    }
    console.log("tamaño: " + tamaño);
}
/*Funcion para mostar el promedio de los elementos 
 del array*/
function getProm(arrNum) {
    let res = 0;
    for (let i = 0; i < arrNum.length; i++) {
        res += arrNum[i];
    }
    return console.log("El promedio es: " + res / arreglo.length);
}

function getMen(arreglo) {
    let valorM = arreglo[0], pos = 0;
    for (let i = 1; i < arreglo.length; i++) {
        if (valorM > arreglo[i]) {
            valorM = arreglo[i];
            pos = i;
        }
    }
    console.log("Valor menor es: " + valorM);
    console.log("Posicion es:" + pos);
} 