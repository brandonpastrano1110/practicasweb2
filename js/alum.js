/*let alumno = [{
    "matricula": "20203023",
    "nombre": "Lopez Acosta Jose Miguel",
        "grupo": "TI-73",
        "carrera": "Tecnologias de la Información",
        "foto": "/img/20203023.jpg",
    }, {
        "matricula": "20203023",
        "nombre": "Lopez Acosta Jose Miguel",
        "grupo": "TI-73",
        "carrera": "Tecnologias de la Información",
        "foto": "/img/20203023.jpg",
    }, {
        "matricula": "20203023",
        "nombre": "Lopez Acosta Jose Miguel",
        "grupo": "TI-73",
        "carrera": "Tecnologias de la Información",
        "foto": "/img/20203023.jpg",
    }
];
console.log("Matricula: " + alumno.matricula);
console.log("Nombre: " + alumno.nombre);
console.log("Grupo: " + alumno.grupo);
alumno.nombre = "Scott S. Mickel";
console.log("Nombre: " + alumno.nombre);

let cuentaBanco = {
    "numero": "10001",
    "banco": "Banorte",
    cliente: {
        "nombre": "Jose Lopez",
        "fecha": "2020-01-01",
        "sexo": "M",
    },
    "saldo": "1000000",
};

console.log("Nombre:" + cuentaBanco.cliente.nombre);
console.log("Saldo:" + cuentaBanco.saldo);
cuentaBanco.cliente.sexo = "F";
*/
let alumno = [{
    "matricula": "2021030652",
    "nombre": "Quezada Lara Jesus Alejandro",
    "grupo": "TI-73",
    "carrera": "Tecnologias de la Informacion",
    "foto": '<img src="/img/1.jpeg">'
}, {
    "matricula": "2021030328",
    "nombre": "Garcia Gonzalez Jorge Enrique",
    "grupo": "TI-73",
    "carrera": "Tecnologias de la Informacion",
    "foto": '<img src="/img/2.jpeg">'
}, {
    "matricula": "2021030652",
    "nombre": "Pastrano Navarro Brandon Rogelio",
    "grupo": "TI-73",
    "carrera": "Tecnologias de la Informacion",
    "foto": '<img src="/img/3.jpeg">'
}, {
    "matricula": "2021030142",
    "nombre": "Aguilar Romero Jonathan Jesus",
    "grupo": "TI-73",
    "carrera": "Tecnologias de la Informacion",
    "foto": '<img src="/img/4.jpeg">'
}, {
    "matricula": "2021030143",
    "nombre": "Tirado Rios Oscar",
    "grupo": "TI-73",
    "carrera": "Tecnologias de la Informacion",
    "foto": '<img src="/img/5.jpeg">'
}, {
    "matricula": "2019030880",
    "nombre": "Quezada Ramos Julio Emiliano",
    "grupo": "TI-73",
    "carrera": "Tecnologias de la Informacion",
    "foto": '<img src="/img/6.jpeg">'
}, {
    "matricula": "2021030143",
    "nombre": "Tirado Rios Oscar",
    "grupo": "TI-73",
    "carrera": "Tecnologias de la Informacion",
    "foto": '<img src="/img/7.jpeg">'
}, {
    "matricula": "2021030311",
    "nombre": "Reyes Lizarraga Jonathan Alexis",
    "grupo": "TI-73",
    "carrera": "Tecnologias de la Informacion",
    "foto": '<img src="/img/8.jpeg">'
}, {
    "matricula": "2021030314",
    "nombre": "Peñaloza Pizarro Felipe Andres",
    "grupo": "TI-73",
    "carrera": "Tecnologias de la Informacion",
    "foto": '<img src="/img/9.jpeg">'
}, {
    "matricula": "2020030321",
    "nombre": "Ontiveros Govea Yair Alejandro",
    "grupo": "TI-73",
    "carrera": "Tecnologias de la Informacion",
    "foto": '<img src="/img/10.jpeg">'
}
];
const alumnosContainer = document.querySelector('.alumnos-container');
let Tabla = document.querySelector('.td_d');

function getTabla(tabla) {
    for (let i = 0; i < alumno.length; i++) {
        let al = document.createElement('tr');
        al.innerHTML =
            `<td>${alumno[i].matricula}</td>
            <td>${alumno[i].nombre}</td>
            <td>${alumno[i].grupo}</td>
            <td>${alumno[i].carrera}</td> 
            <td><img src="${alumno[i].foto}" alt="${i}"></img></td>
            `; 
        Tabla.appendChild(al);
    }
}
getTabla(Tabla);

/*
console.log(productos[0].precio);
productos[1].descripcion = "Hola";

for (let i = 0; i < productos.length; i++) {
    console.log("Productos:" + productos[i].codigo);
    console.log(productos[i].descripcion);
    console.log(productos[i].precio);
}*/