let alumnos = [{
    "matricula":"2021030205",
    "nombre":"Pastrano Navarro Brandon Rogelio",
    "grupo":"TI-73",
    "carrera":"Tecnologias de la Informacion",
    "foto":'<img src="/img/12.jpeg">'
},{
    "matricula":"2021030142",
    "nombre":"Aguilar Romero Jonathan Jesus",
    "grupo":"TI-73",
    "carrera":"Tecnologias de la Informacion",
    "foto":'<img src="/img/11.jpeg">'
},{
    "matricula":"2021030652",
    "nombre":"Quezada Lara Jesus Alejandro",
    "grupo":"TI-73",
    "carrera":"Tecnologias de la Informacion",
    "foto":'<img src="/img/10.jpeg">'
},{ 
    "matricula":"2021030328",
    "nombre":"Garcia Gonzalez Jorge Enrique",
    "grupo":"TI-73",
    "carrera":"Tecnologias de la Informacion",
    "foto":'<img src="/img/7.jpeg">'
},{
    "matricula":"2021030143",
    "nombre":"Solis Velarde Oscar",
    "grupo":"TI-73",
    "carrera":"Tecnologias de la Informacion",
    "foto":'<img src="/img/2.jpeg">'
},{
    "matricula":"2019030880",
    "nombre":"Quezada Ramos Julio Emiliano",
    "grupo":"TI-73",
    "carrera":"Tecnologias de la Informacion",
    "foto":'<img src="/img/6.jpeg">'
},{
    "matricula":"2021030149",
    "nombre":"Arias Tirado Mateo",
    "grupo":"TI-73",
    "carrera":"Tecnologias de la Informacion",
    "foto":'<img src="/img/5.jpeg">'
},{
    "matricula":"2021030311",
    "nombre":"Reyes Lizarraga Jonathan Alexis",
    "grupo":"TI-73",
    "carrera":"Tecnologias de la Informacion",
    "foto":'<img src="/img/1.jpeg">'
},{
    "matricula":"2021030314",
    "nombre":"Peñaloza Pizarro Felipe Andres",
    "grupo":"TI-73",
    "carrera":"Tecnologias de la Informacion",
    "foto":'<img src="/img/8.jpeg">'
},{
    "matricula":"2020030321",
    "nombre":"Ontiveros Govea Yair Alejandro",
    "grupo":"TI-73",
    "carrera":"Tecnologias de la Informacion",
    "foto":'<img src="/img/4.jpeg">'
}
];
 

const alumTab=document.getElementById('alum-table');
for(let i=0; i<alumnos.length;i++){
    let newRow= document.createElement('tr');
    newRow.innerHTML=
        `
        <td>${alumnos[i].matricula}</td>
        <td>${alumnos[i].nombre}</td>
        <td>${alumnos[i].grupo}</td>
        <td>${alumnos[i].carrera}</td>
        <td>${alumnos[i].foto}</td>
        `;
    alumTab.appendChild(newRow);
};