const btnCalcular = document.getElementById('btnCalcular');



btnCalcular.addEventListener("click", function () {
    let estatura = parseFloat(document.getElementById('Estatura').value);
    let peso = parseFloat(document.getElementById('Peso').value);
    let edad = parseInt(document.getElementById('Edad').value);
    let Tabla = document.getElementById('Tabla');
    let celda = Tabla.rows[1].cells[0];
    if (estatura > 0 && peso > 0 && edad>0) {
        if (estatura < 2.5) {
            let imc = peso / (estatura * estatura);
            document.getElementById('Res').value = imc.toFixed(2);
            celda.textContent = getEdadR(edad);
            celda = Tabla.rows[1].cells[1];
            celda.textContent = getCalHom(edad, peso).toFixed(2)+" cal.";
            celda = Tabla.rows[1].cells[2];
            celda.textContent = getCalMuj(edad, peso).toFixed(2)+" cal.";
        }
        else {
            alert("Escribe la estatura en Metros (0.00)");
        }
    }
    else {
        alert("Rellena todos los datos");
    }
});
function limpiar() {
    let parrafo = document.getElementById("pa");
    parrafo.innerHTML = "";
}
function getCalHom(edad, peso) {
    if (edad >= 10 && edad < 18) {
        return (17.686 * peso) + 658.2;
    }
    else if (edad >= 18 && edad < 30) {
        return (15.057 * peso) + 692.2;
    }
    else if (edad >= 30 && edad < 60) {
        return (11.472 * peso) + 873.1;
    }
    else if (edad >= 60) {
        return (11.711 * peso) + 587.7;
    }
}
function getCalMuj(edad, peso) {
    if (edad >= 10 && edad < 18) {
        return (13.384 * peso) + 692.6;
    }
    else if (edad >= 18 && edad < 30) {
        return (14.818 * peso) + 486.6;
    }
    else if (edad >= 30 && edad < 60) {
        return (8.126 * peso) + 845.6;
    }
    else if (edad >= 60) {
        return (9.082 * peso) + 658.5;
    }
}
function getEdadR(edad) {
    if (edad >= 10 && edad < 18) {
        return "10->18";
    }
    else if (edad >= 18 && edad < 30) {
        return "18->30";
    }
    else if (edad >= 30 && edad < 60) {
        return "30->60";
    }
    else if (edad >= 60) {
        return "60->";
    }
}