/* declarar variables */
const btnCalcular = document.getElementById('btnCalcular');
btnCalcular.addEventListener("click", function () {

    let valorAuto = document.getElementById('ValorAuto').value;
    let pInicial = document.getElementById('Porcentaje').value;
    let plazos = document.getElementById('Plazos').value;
    if (valorAuto > 0 && pInicial > 0) {
        //calculos
        let pagoInicial = valorAuto * (pInicial / 100);
        let total = valorAuto - pagoInicial;
        let pagoMen = total / plazos;
        //mostrar
        document.getElementById('PagoInicial').value = "$" + pagoInicial.toFixed(2);
        document.getElementById('TotalFin').value = "$" + total.toFixed(2);
        document.getElementById('PagoMensual').value = "$" + pagoMen.toFixed(2);
    }
    else {
        alert('Llena todos los datos...')
    }
});
function limpiar() { 
    let uni = document.getElementById("Uni");
    let paInicial = document.getElementById("PagoInicial");
    let toFin = document.getElementById("TotalFin");
    let paMensual = document.getElementById("PagoMensual"); 
    uni.textContent += " | " + paInicial.value + "    " + toFin.value + "   " + paMensual.value + " | ";
    paInicial.value = "";
    toFin.value = "";
    paMensual.value = "";    
}